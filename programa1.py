# Programa 1. Genera una lista del 1 al 100 en la consola.
# Commit 1: Primer commit. Generacion del proyecto
# Commit 2: Se añade la funcionalidad de preguntarle al usuario el rango de la lista
# Commit 3: Se añade la opcion de imprimir el resultado en un archivo de texto
# Commit 4: Se añade la opcion de que el usuario elija el nombre del archivo y se corrige el defecto que cuando a > b no imprime la lista y que imprime en el archivo si la s es miniscula
# Commit 5: Se añade la opcion de que usuario pueda imprimir la lista tanto en consola como en un archivo.
# Commit 6: Se añade el manejo de errores en los inputs

# Funcion que genera la lista
def lista(num_a,num_b,imprime,archivo):
    if(num_a > num_b):
        incremento = -1
    else:
        incremento = 1
    for i in range(num_a,num_b + incremento,incremento):
        imprime(i,archivo)

def imprime_consola(cadena,archivo):
    print(cadena)

def imprime_archivo(cadena,archivo):
    archivo.write("%s\n" % cadena)

#Manejo de errores
try:
    a = int(input("Ingrese el numero con el que desea empezar: "))

    b = int(input("Ingrese el numero con el que desea terminar: "))
except:
    print("Error, algun input no es numerico.")
    exit(0)

print("Donde quiere ver la lista?")
print("F: Archivo")
print("C: Consola")
print("A: Ambos")
eleccion = input("R= ")

if (eleccion.upper() == 'F'):
    nombre = input("Ingrese el nombre del archivo, si no elige uno sea default: ")
    if(nombre == ''):
        nombre = 'default'
    nombre += '.txt'
    fp = open(nombre,"w")
    imp = imprime_archivo
    lista(a,b,imp,fp)
elif (eleccion.upper() == 'A'):
    nombre = input("Ingrese el nombre del archivo, si no elige uno sea default: ")
    if(nombre == ''):
        nombre = 'default'
    nombre += '.txt'
    fp = open(nombre,"w")
    imp = imprime_archivo
    lista(a,b,imp,fp)
    fp = None
    imp = imprime_consola
    lista(a,b,imp,fp)
elif (eleccion.upper() == 'C'):
    fp = None
    imp = imprime_consola
    lista(a,b,imp,fp)
else:
    print("Error, opcion no esta en el menu")

# Se espera un enter para continuar
input("Presione enter para continuar.") 
